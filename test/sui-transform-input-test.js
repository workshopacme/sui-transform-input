var assert = require("assert");
var React = require('react');
var TestUtils = require('react/lib/ReactTestUtils');
var expect = require('expect');
var TransformInput = require('../src/sui-transform-input');

describe('sui-transform-input component test suite', function () {
  it('loads without problems', function () {
    assert.notEqual(undefined, TransformInput);
  });

  it('renders into document', function() {
    var root = TestUtils.renderIntoDocument(<TransformInput />);
    expect(root).toExist();
  });
});
