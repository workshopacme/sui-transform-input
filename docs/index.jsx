import React from 'react';
import {TransformInput} from '../src';
import '../src/index.scss';

const translateToCatalan = function(value) {
  const host = 'https://www.softcatala.org/apertium/json/translate';
  const url = `${host}?langpair=es%7Cca&q=${value.replace(/\*/g, '')}&markUnknown=yes&key=NmQ3NmMyNThmM2JjNWQxMjkxN2N`;
  return fetch(url)
        .then(response => response.json())
        .then(translate => translate.responseData.translatedText);
};

React.render(<TransformInput transform={(value) => value.toUpperCase()}/>, document.getElementById('upperCase'));
React.render(<TransformInput transform={(value) => value.replace(/\./g, '').split('').join('.')}/>, document.getElementById('dots'));
React.render(<TransformInput transform={translateToCatalan}/>, document.getElementById('catalan'));
