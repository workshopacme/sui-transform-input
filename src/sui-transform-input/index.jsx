import React from 'react';

const defaultTransform = function(value) {
  return value;
};

export default class TransformInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
  }

  handleChange(event) {
    const value = (this.props.transform || defaultTransform).call(this, event.target.value);

    if (value.then && typeof value.then === 'function') {
      value.then(result => this.setState({value: result}));
    } else {
      this.setState({value});
    }
  }

  render() {
    return (
      <div className='sui-transform-input'>
        <input
          className='sui-transform-input__input'
          type='text'
          value={this.state.value}
          onChange={this.handleChange.bind(this)}/>
      </div>
    );
  }
}

TransformInput.propTypes = {transform: React.PropTypes.func};
