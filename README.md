# Transform Input

Sample component to show how to use the generator to create a new generic component.This component is a special input that applies a transformation over its value.

## API

To use this component you must set the `transform` property. This property is a function that has a string parameter and returns another string with the transformation applied.

Ex.:

```javascript
 React.render(<TransformInput transform={(value) => value.toUpperCase()}/>, document.getElementById('upperCase'));
```

By default, the identity transformation is applied.